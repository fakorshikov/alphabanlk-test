package ru.testing.gateway;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import ru.testing.entities.Translations;
import ru.testing.entities.Request;

public class Gateway {
    private static final String URL = "https://translate.api.cloud.yandex.net/translate/v2/translate";
    private static final String token = "t1.9euelZrLyZeUk8ack4yPxo_Km42TmO3rnpWazZzLmM3OjY3NyJvJycqWm8nl8_cwQVR3-e96QxMs_t3z93BvUXf573pDEyz-.rZS1fTqAWRDEDaHwM9j8nSvDBf2mqeMCEkcEvzJrG33eS1sTqbvzzxpKe9yY1kEWYxXuMFy6EDPek4SOqlMaDg";
    private static final String folderId = "b1gqhfbgkpjq4dvr375a";

    public Translations getTranslations(String sourceLanguageCode, String targetLanguageCode, String format, String text) throws UnirestException
    {

        Gson gson = new Gson();
        Request request = new Request(sourceLanguageCode, targetLanguageCode, format, text, folderId);
        String reqJson = gson.toJson(request);

        System.out.println(reqJson);

        HttpResponse<String> response = Unirest.post(URL)
                .header("Authorization", "Bearer " + token)
                .header("Content-Type", "application/json")
                .body(reqJson)
                .asString();

        String responseBody = response.getBody();

        System.out.println(responseBody);

        return gson.fromJson(responseBody, Translations.class);

    }
}
