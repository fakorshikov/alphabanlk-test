package ru.testing.entities;
import com.google.gson.annotations.SerializedName;

public class Request {

    @SerializedName("sourceLanguageCode")
    public String sourceLanCode;

    @SerializedName("targetLanguageCode")
    public String tarLanCode;

    @SerializedName("format")
    public String format;

    @SerializedName("texts")
    public String texts;

    @SerializedName("folderId")
    public String folderId;


    public Request(String sourceLanCode, String tarLanCode, String format, String texts, String folderId) {
        this.sourceLanCode = sourceLanCode;
        this.tarLanCode = tarLanCode;
        this.format = format;
        this.texts = texts;
        this.folderId = folderId;
    }

}

