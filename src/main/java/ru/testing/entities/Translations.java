package ru.testing.entities;

import com.google.gson.annotations.SerializedName;

public class Translations {

    @SerializedName("translations")
    public Translation[] translations;

    public Translation[] getTranslations() {
        return translations;
    }

}
