package ru.testing.entities;

import com.google.gson.annotations.SerializedName;

public class Translation {

    @SerializedName("text")
    public String text;

    @SerializedName("detectedLanguageCode")
    public String langCode;

    public String getText() {
        return text;
    }

    public String getLangCode() {
        return langCode;
    }

}
