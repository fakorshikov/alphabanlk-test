package ru.testing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ru.testing.entities.Translations;
import ru.testing.gateway.Gateway;
import ru.testing.entities.Translation;

public class YandexTranslateTest {
    private static final String SOURCE_LANG= "en";
    private static final String TARGET_LANG = "ru";
    private static final String FORMAT = "PLAIN_TEXT";
    private static final String TEXT = "Hello world!";

    private static final String RESULT = "Всем привет!";

    //@Test
    //@DisplayName("")
    public void testTranslation(){
        Gateway yandexTranslateGateway = new Gateway();
        try {
            Translations translations = yandexTranslateGateway.getTranslations(SOURCE_LANG, TARGET_LANG, FORMAT, TEXT);
            //Assertions.assertEquals(translations.getTranslations()[0].getText(), RESULT);
        } catch (Exception e) {
            System.out.println( e.toString() );
            return;
        }
    }
    public static void main (String[] args){
        YandexTranslateTest test = new YandexTranslateTest();
        test.testTranslation();

    }
}
